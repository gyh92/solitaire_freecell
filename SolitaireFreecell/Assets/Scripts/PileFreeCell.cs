﻿using UnityEngine;
using System.Collections.Generic;

public class PileFreeCell : Pile {

	public PileFreeCell(){

		pileOfCards = new List<Card> ();

	}

	public override Card CardOnTop ()
	{
		if (pileOfCards.Count > 0)
			return pileOfCards [pileOfCards.Count - 1];
		else
			return null;
	}

	public override bool ValidAddingCard(Card card)

	{
		if (pileOfCards.Count == 0) {
			return true;
		}
		else
			return false;
	}

}

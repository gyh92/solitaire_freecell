﻿using UnityEngine;
using System.Collections.Generic;

public class PileColumn : Pile {

	public PileColumn(){
		
		pileOfCards = new List<Card> ();
	
	}
	public override Card CardOnTop ()
	{
		if (pileOfCards.Count > 0)
			return pileOfCards [pileOfCards.Count - 1];
		else
			return null;
	}

	public override bool ValidAddingCard(Card card)

	{
//		Debug.Log (CardOnTop().getColor());
		if ((pileOfCards.Count == 0) ||
			((int)(CardOnTop().face) - 1 == (int)card.face && CardOnTop().getColor() != card.getColor())) {
			return true;
			}
		else
			return false;
	}

}
//
//if ((pileOfCards.Count == 0) ||
//	((int)(base.CardOnTop.face) - 1 == (int)card.face && base.CardOnTop.getColor() != card.getColor())) {
//	return true;
//}
﻿using UnityEngine;
using System.Collections.Generic;

public class PileFundation : Pile {

	public PileFundation(){

		pileOfCards = new List<Card> ();

	}

	public override Card CardOnTop ()
	{
		if (pileOfCards.Count > 0)
			return pileOfCards [pileOfCards.Count - 1];
		else
			return null;
	}

	public override bool ValidAddingCard(Card card)

	{
		if ((pileOfCards.Count == 0) && (card.face == CardFace.ACE)) {
			return true;
		}

		if (pileOfCards.Count > 0) {
			Card topCard = CardOnTop();
			if (topCard.suit == card.suit && ((int)topCard.face + 1 == (int)card.face)) {
				return true;
			}
		}
			return false;

 	}
}

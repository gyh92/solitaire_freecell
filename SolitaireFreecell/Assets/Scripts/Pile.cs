﻿using UnityEngine;
using System.Collections.Generic;

public class Pile : MonoBehaviour {

	public List<Card> pileOfCards;

	public Pile(){
		pileOfCards = new List<Card> ();
	}

	public virtual Card CardOnTop() 
	{
		
			if (pileOfCards.Count > 0)
				return pileOfCards [pileOfCards.Count - 1];
			else
				return null;
		
	}

	public int Size(){
		return pileOfCards.Count;
	}

	public void ForceAddCard(Card card){
		pileOfCards.Add (card);
	}

	public void ForceRemoveCard(){
		if(pileOfCards.Count > 0)
			pileOfCards.RemoveAt (pileOfCards.Count - 1);
	}

	public void AddCard(Card card){
		if (ValidAddingCard (card)) {
			pileOfCards.Add (card);
			Debug.Log ("Add card succesfully");
		}
		else
		Debug.Log ("Add card failed");

	}

	public void RemoveCard(){
		if(pileOfCards.Count > 0)
			pileOfCards.RemoveAt (pileOfCards.Count - 1);
	}

	public void Shuffle(){


		Card CurrentCard;
		Debug.Log ("Ready to shuffle ");
		Debug.Log (pileOfCards.Count);
		for (int i = 0; i < pileOfCards.Count - 1; i++) {

			int randomNum = (int) Random.Range (0, pileOfCards.Count - 1);
			CurrentCard = pileOfCards [randomNum];
			pileOfCards [randomNum] = pileOfCards [i];
			pileOfCards [i] = CurrentCard;
			//Debug.Log ("Shuffled "  + randomNum + " with " + i);
		}
	}

	public Card CreateCard(CardSuit suit, CardFace face, int color, bool cardVisible){
		Card card = (Instantiate(GameManager.Instance.CardPrefab) as GameObject).GetComponent<Card>();
		card.suit = suit;
		card.face = face;
		card.color = color;
		card.cardVisible = cardVisible;
		return card;
	}


	public void CreateCards(bool cardVisible)
	{
		int numberOfSuits = 4;
		for (int suit = 0; suit < numberOfSuits; suit++)
		{
			var realSuit = (suit % 4) + 1;
			for (int number = 1; number <= 13; number++)
			{
				Card card = (Instantiate(GameManager.Instance.CardPrefab) as GameObject).GetComponent<Card>();
				card.suit = (CardSuit)realSuit;
				card.face = (CardFace)number;
				if (suit == (int)CardSuit.SPADES|| suit == (int)CardSuit.CLUBS) {
					card.color = 1;
					//Debug.Log ("black card spades or clubs");
				}
				if (suit == (int)CardSuit.HEARTS || suit == (int)CardSuit.DIAMONDS) {
					card.color = 0;
					//Debug.Log ("red card hearts or diamonds");
				}
				card.cardVisible = cardVisible;
				this.AddCard (card);
			}
		}

	}

	public virtual bool ValidAddingCard(Card card){
		return true;
	}


	public void TestCards(){
		
		for (int i = 0; i < pileOfCards.Count; i++) {
			Debug.Log (pileOfCards [i].suit + " " + pileOfCards [i].face + " " + pileOfCards [i].color);

		}
	}

}

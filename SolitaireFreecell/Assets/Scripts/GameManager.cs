﻿using UnityEngine;
using System.Collections;

public class GameManager : MonoBehaviour {

	public static GameManager Instance;
	
	public UnityEngine.Object CardPrefab;
	public UnityEngine.Object PilePrefab;

	public UnityEngine.Object PileFoundationPrefab;
	public UnityEngine.Object PileFreeCellPrefab;
	public UnityEngine.Object PileColumnPrefab;

	public GameManager()
	{
		Instance = this;
	}
	public void Awake()
	{
		
	}

}
